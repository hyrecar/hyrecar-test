(function(angular){
	'use strict';

    angular.module('HyreCarAppTest', [])
        .config()
		.run();

})(angular);;