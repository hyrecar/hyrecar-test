var config = {
    gruntInit: {
        connect: {
            options: {
                host: 'localhost',
                port: 80,
                open:'http://admin.local.hyrecar.com'
            },
            livereload: {
                options: {
                    open: undefined
                }
            }
        }
    }
};

module.exports = config;