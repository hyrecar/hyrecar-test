<?php

require_once SITE_PATH .'/inc/etc/CORSMiddleware.php';
require_once SITE_PATH .'/inc/login/JWTAuthenticationMiddleware.php';
require_once SITE_PATH .'/inc/login/PasswordHashClass.php';

	$slim_config = [

		'debug' => true,
		'log.enabled' => true,
		'log.level' => \Slim\Log::DEBUG,
		'cookies.encrypt' => false,
		'cookies.lifetime' => '5 minutes',
		'cookies.path' => '/',
		'cookies.domain' => CURR_DOMAIN,
		'http.version' => '1.0',
		'cache.path' => SITE_PATH .'/cache',
		'templates.path' => SITE_PATH .'/templates'

	];

use Chatwork\JsonRequestMiddleware;
use \Needcaffeine\Slim\Extras\Views\ApiView;
use \Needcaffeine\Slim\Extras\Middleware\ApiMiddleware;

$app = new \Slim\Slim( $slim_config );
