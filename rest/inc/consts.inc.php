<?php

namespace HyreCar;

define('DATE_MEDIUM_FORMAT','F d');
define('TIME_FORMAT','H:i:s');

define('PRICE_INSURANCE_CENTS_PERDAY',1000);
define('PRICE_FEE_PERCENTS',10);
define('PRICE_UNLOCK_CENTS',3900);
define('PRICE_MVR_CHECK',2999);
define('PRICE_INTEREST_PERCENTS',15);
define('PRICE_LATE_RENTAL_FEE',5000);
define('PRICE_HOLD',20000);

define('ENCRYPTION_KEY', 'w03By6KahHvrv2aAYs731ByQVKi4uWcC');
define('ENCRYPTION_KEY_LICENSE', 'w03By6KahHvrv2aAYs731ByQVKi4uWcC');


// Dropoff window width
// All dropoffs within interval
// [ $predicted_dropoff_date - DROPOFF_WINDOW_HRS; $predicted_dropoff_date + DROPOFF_WINDOW_HRS ]
// won't be charged for late dropoff and won'be shrinked.
define('DROPOFF_WINDOW_HRS', 6);

// Minimal gap prior application
define('GAP_RENTAL_START_DAYS', 1);

define('BILLING_TRANSACTION_CHARGE','charge');
define('BILLING_TRANSACTION_TRANSFER','transfer');
define('BILLING_TRANSACTION_REFUND','refund');

define('BILLING_ITEM_RENT_PERSONAL','rent_personal');
define('BILLING_ITEM_RENT_PERSONAL_OVERDUE','rent_personal_overdue');
define('BILLING_ITEM_RENT_PERSONAL_CHARGEBACK','rent_personal_chargeback');
define('BILLING_ITEM_RENT_PERSONAL_ADDITIONAL_PAYMENT','rent_personal_additional_payment');
define('BILLING_ITEM_RENT_PERSONAL_EXTENSION','rent_personal_extension');
define('BILLING_ITEM_RENT_PERSONAL_EXTENSION_OVERDUE','rent_personal_extension_overdue');
define('BILLING_ITEM_RENT_PERSONAL_EXTENSION_CHARGEBACK','rent_personal_extension_chargeback');
define('BILLING_ITEM_RENT_PERSONAL_EXTENSION_ADDITIONAL_PAYMENT','rent_personal_extension_additional_payment');
define('BILLING_ITEM_RENT_LIVERY','rent_livery');
define('BILLING_ITEM_RENT_LIVERY_OVERDUE','rent_livery_overdue');
define('BILLING_ITEM_RENT_LIVERY_CHARGEBACK','rent_livery_chargeback');
define('BILLING_ITEM_RENT_LIVERY_ADDITIONAL_PAYMENT','rent_livery_additional_payment');
define('BILLING_ITEM_RENT_LIVERY_EXTENSION','rent_livery_extension');
define('BILLING_ITEM_RENT_LIVERY_EXTENSION_OVERDUE','rent_livery_extension_overdue');
define('BILLING_ITEM_RENT_LIVERY_EXTENSION_CHARGEBACK','rent_livery_extension_chargeback');
define('BILLING_ITEM_RENT_LIVERY_EXTENSION_ADDITIONAL_PAYMENT','rent_livery_extension_additional_payment');
define('BILLING_ITEM_FINE','fine');
define('BILLING_ITEM_UNLOCK_DRIVER','unlock_driver');
define('BILLING_ITEM_MVR_CHECK','mvr_check');
define('BILLING_ITEM_MVR_CHECK_CHARGEBACK','mvr_check_chargeback');
define('BILLING_ITEM_WITHDRAW','withdraw');
define('BILLING_ITEM_CHARGEBACK','chargeback');
define('BILLING_ITEM_ADDITIONAL_PAYMENT','additional_payment');

define('BILLING_TYPE_INCOME','income');
define('BILLING_TYPE_EXPENSE','expense');
define('BILLING_TYPE_REFUND','refund');
define('BILLING_TYPE_DEDUCTION','deduction'); // Deduction of income.
define('BILLING_TYPE_WITHDRAW','withdraw');

define('BILLING_STATUS_DRAFT','draft');
define('BILLING_STATUS_ON_BALANCE','on_balance');
define('BILLING_STATUS_ARCHIVED','archived');

define('ACCESS_SELECT_RESTRICTED',0);
define('ACCESS_SELECT_LIMITED',1);
define('ACCESS_SELECT_FULL',2);
define('ACCESS_SELECT_ADMIN',4);

define('USER_DRIVER',0);
define('USER_OWNER_LIVERY',1);
define('USER_OWNER_P2P',2); // not used now

define('USER_UNVERIFIED',0);
define('USER_VERIFIED',1);
define('USER_REJECT',2);

define('USER_ROLE_FREE',0);
define('USER_ROLE_PREMIUM',1);

define('USER_IMG_SIZE', 400);
define('USER_IMG_THUMB_SIZE', 100);

define('THUMB_SUFFIX', 'thumb_');

define('STATUS_REJECT', 2);
define('STATUS_ACTIVE', 1);
define('STATUS_DEACTIVE', 0);

define('CAR_LICENSING_PERSONAL','personal');
define('CAR_LICENSING_COMMERCIAL','commercial');
define('CAR_LICENSING_LIVERY','livery');
define('CAR_LICENSING_LEASING','leasing');
define('CAR_LICENSING_PERSONAL_I',0);
define('CAR_LICENSING_COMMERCIAL_I',1);
define('CAR_LICENSING_LIVERY_I',2);
define('CAR_LICENSING_LEASING_I',3);

define('BG_CHECK_NOTAPPLIED','notapplied');
define('BG_CHECK_APPLIED','applied');
define('BG_CHECK_READY','ready');
define('BG_CHECK_CLEAR','clear');
define('BG_CHECK_CONSIDER','consider');
define('BG_CHECK_NOTAPPLIED_I',0);
define('BG_CHECK_APPLIED_I',1);
define('BG_CHECK_REJECTED_I',2);

define('BG_CHECK_APPROVE','approve');
define('BG_CHECK_APPROVED','approved');
define('BG_CHECK_REJECT','reject');
define('BG_CHECK_REJECTED','rejected');
define('BG_CHECK_PENDING','pending');
define('BG_CHECK_DISPUTE','dispute');
define('BG_CHECK_SUSPENDED','suspended');

define('REJECTION_TYPE_CC_DECLINE',1);
define('REJECTION_TYPE_CANCELED',2); // driver cancel app
define('REJECTION_TYPE_REJECTED',3); // car owner reject app
define('REJECTION_TYPE_APPROVED',4); // reject other car's apps
define('REJECTION_TYPE_CRON',5); // reject apps after 24 hours
define('REJECTION_TYPE_EXCEPTED',6); // reject other driver's apps
define('REJECTION_TYPE_SWITCH_CAR',7); // reject application when owner switch car

define('APPLICATION_STATUS_CREATED',0);
define('APPLICATION_STATUS_ANSWERED',1);
define('APPLICATION_STATUS_APPROVED',2);
define('APPLICATION_STATUS_REJECTED',3);
define('APPLICATION_STATUS_REMOVED',4);

define('OFFER_STATUS_ACTIVE',1);
define('OFFER_STATUS_REMOVED',0);
define('OFFER_STATUS_RENTED',2);

define('RENTAL_STATUS_WAITING_FOR_PAYMENT',4); // Waiting for user payment
define('RENTAL_STATUS_WAITING_FOR_INSURANCE',5); // Waiting for insurance
define('RENTAL_STATUS_WAITING_FOR_CONFIRMATION',6); // Waiting for driver' and owner' confirmation
define('RENTAL_STATUS_CANCELLED_BY_OWNER',1); // Cancelled by owner
define('RENTAL_STATUS_CANCELLED_BY_DRIVER',2); // Cancelled by driver
define('RENTAL_STATUS_ACTIVE',0); // Rental is active
define('RENTAL_STATUS_FINISHED',7); // Rental is finished. No payments were made yet
define('RENTAL_STATUS_RECREATED_BY_CRON',3); // deprecated
define('RENTAL_STATUS_ARCHIVED',8); // Payments were made for this rental. Archive it.

define('RENTAL_EXTENDED',1); // Driver extend rental
define('RENTAL_NOT_EXTEND',0); // Driver not extend rental

define('CAR_FRONT_PHOTO','car_front_photo');
define('CAR_PASSENGER_SIDE_PHOTO','car_passenger_side_photo');
define('CAR_BACK_PHOTO','car_back_photo');
define('CAR_DRIVER_SIDE_PHOTO','car_driver_side_photo');

define('EMAIL_SENT',1); // sent email
define('EMAIL_NOT_SEND',0); // not send email

define('DRIVER_REFERRAL',10);
define('PARTNER_REFERRAL',20);

// Errors
define('S_OK',0);
define('S_ERROR',-1);

define('ERROR_USER_NO_PAYMENT_ACCOUNT',-10001);
define('ERROR_USER_NO_MVR_CHECKED',-10002);
define('ERROR_USER_NO_SCREENSHOTS',-10003);
define('ERROR_USER_MVR_CONSIDER',-10004);

define('ERROR_LOGIN_WRONG_EMAIL',-10010);
define('ERROR_LOGIN_WRONG_PASSWORD',-10011);
define('ERROR_LOGIN_ACCOUNT_LOCKED',-10012);
define('ERROR_LOGIN_ACCOUNT_UNVERIFIED',-10013);

define('NOTIFICATION_RENTAL_ABOUT_OVER_HOURLY',1);
define('NOTIFICATION_RENTAL_ABOUT_OVER_5HRS',2);
define('NOTIFICATION_RENTAL_ABOUT_OVER_DAILY',3);

define('NOTIFICATION_RATING', 'rating');
define('NOTIFICATION_STRIPE', 'stripe');

define('SCREENSHOT_UBER', 'uber');
define('SCREENSHOT_LICENSE', 'license');

define('STMN_DSCR_HDR', 'HYRC ');

// Array Encryption
define('USEROP_ENC_ALG', MCRYPT_DES );
define('USEROP_ENC_KEY', '16ba323a67517111');
define('USEROP_ENC_IV', 'af24e921e1adb638' );
define('USEROP_BASE64_DEFAULT_ALPHABET', 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/' );
define('USEROP_BASE64_CUSTOM_ALPHABET', 'Xby-sxpjnulT0qUhCwDgtmYdL5fRZGNeI7oiQzHakJ3A2K61rW9vOBSMEFVc_84P' );

// New automated emails
define('SEND_MAIL_REMAIND_EXTEND', '1');
define('SEND_MAIL_REMAIND_RENTAL_AFTER_RENTED_TYPE_1', 2);
define('SEND_MAIL_REMAIND_RENTAL_AFTER_RENTED_TYPE_1_DAY', 14);
define('SEND_MAIL_REMAIND_RENTAL_AFTER_RENTED_TYPE_2', 3);
define('SEND_MAIL_REMAIND_RENTAL_AFTER_RENTED_TYPE_2_DAY', 30);
define('SEND_MAIL_REMAIND_RENTAL_AFTER_RENTED_TYPE_3', 4);
define('SEND_MAIL_REMAIND_RENTAL_AFTER_RENTED_TYPE_3_DAY', 60);
define('SEND_MAIL_REMAIND_THE_FIRST_RENTAL', 5);
define('SEND_MAIL_REMAIND_THE_FIRST_RENTAL_DAY', 7);
define('THE_FIRST_SEND_MAIL_DEFAULT', 0);
define('THE_FIRST_SEND_MAIL_SENT', 1);
define('DEFAULT_DATE_DT', '0000-00-00 00:00:00');

//time sendmail expire
define('TIME_CHECK_BACKGROUND', 86400);
define('STATUS_CHECK_BACKGROUND_SENT', 0);
define('STATUS_CHECK_BACKGROUND_NOT_SENT', 1);

//manage file template
define('TEMPLATE_TYPE_EMAIL', 0);
define('TEMPLATE_TYPE_SMS', 1);
// encrypty Image
define('ENCRYPT_IMAGE_LICENSE', 'license');
define('STATUS_ENCRYPT_LICENSE', 1);
define('STATUS_ENCRYPT_IMAGE', 1);
//hyr 673 update contract
define('TYPE_CONTRACT_ALL', 0);
define('TYPE_CONTRACT_EXTEND', 1);
define('TYPE_CONTRACT_ORIGIN', 2);
define('NOTIFY_RENTAL_STATUS_HIDDEN', 0);
define('NOTIFY_RENTAL_STATUS_SHOW', 1);

// hyr 797 Authorization and capture
define('STRIPE_CAPTURE_FALSE', 0);
define('STRIPE_CAPTURE_TRUE', 1);
define('STRIPE_CAPTURE_TRANSACTION_TYPE', 1);
define('STRIPE_CAPTURE_TRANSACTION_TYPE_RELEASED', 2);
define('STRIPE_CAPTURE_SUCCESS', 'succeeded');

//hyr-761 : integrate payment to zoho crm
define('ZOHO_CRM_ACTION_INSERT', '1');
define('ZOHO_CRM_ACTION_UPDATE', '2');
// BEGIN HYR-191: Auto assign Sales Emails from ZenDesk to Zoho sales agent
define('ZOHO_CRM_ACTION_SEARCH', '3');
define('ZOHO_CRM_ACTION_GET_BY_ID', '4');
define('ZOHO_CRM_ACTION_GET_USER', '5');
define('SUCCESS', 'success');
define('FAILURE', 'failure');
// End HYR-191
define('ZOHO_STRIPE_RELEASED', 1);
define('ZOHO_STRIPE_UNRELEASED', 0);


//HYR-904 optimize sort search
define('SORT_TYPE_LAST_ACTIVE', 1);
define('SORT_TYPE_LOCALTION', 2);
define('SORT_TYPE_WEEKLY_PRICE', 3);
define('SORT_TYPE_PRIOR_RENTAL', 4);

// hyr 837 Email templates should have an Active/Inactive status on them
define('EMAIL_SMS_TEMPLATE_ACTIVE',1);
define('EMAIL_SMS_TEMPLATE_INACTIVE',0);
define('EMAIL_MAILINATOR', 'mailinator.com');

define('P2P_19_POINT_INSPECTION_PASS',1);
define('P2P_19_POINT_INSPECTION_FAIL',0);

//HYR-179 define status for owner
define('OWNER_STATUS_HOME_PAGE_REGISTRATION', 'Home Page Registration');
define('OWNER_STATUS_CAR_PENDING_VERIFICATION', 'Car Pending Verification');
define('OWNER_STATUS_CAR_FAILED_VERIFICATION', 'Car Failed Verification');
define('OWNER_STATUS_VEHICLE_LISTED', 'Vehicle Listed');
define('OWNER_STATUS_INVITED_DRIVERS', 'Invited Drivers');
define('OWNER_STATUS_CURRENT_APPLICATION', 'Current Application');
define('OWNER_STATUS_OWNER_REJECTED', 'Owner Rejected');
define('OWNER_STATUS_OWNER_ACCEPTED', 'Owner Accepted');
define('OWNER_STATUS_APPLICATION_REJECTED', 'Application Rejected');

//HYR 179 define status for driver
define('DRIVER_STATUS_REGISTERED', 'Registered');
define('DRIVER_STATUS_STEP_2', 'Step 2');
define('DRIVER_STATUS_STEP_3', 'Step 3');
define('DRIVER_STATUS_SUBMIT_MVR', 'Submit MVR');
define('DRIVER_STATUS_FAILED_MVR', 'Failed MVR');
define('DRIVER_STATUS_PASSED_MVR', 'Passed MVR');
define('DRIVER_STATUS_CURRENT_APPLICATION', 'Current Application');
define('DRIVER_STATUS_OWNER_REJECTED', 'Owner Rejected');
define('DRIVER_STATUS_OWNER_ACCEPTED', 'Owner Accepted');
define('DRIVER_STATUS_PENDING_INSURANCE', 'Pending Insurance');
define('DRIVER_STATUS_INSURANCE_GENERATED', 'Insurance Generated');
define('DRIVER_STATUS_PENDING_PICKUP', 'Pending Pickup');
define('DRIVER_STATUS_ACTIVE_RENTAL', 'Active Rental');
define('DRIVER_STATUS_LATE_RENTAL', 'Late Rental');
define('DRIVER_STATUS_ACCIDENT', 'Accident');
define('DRIVER_STATUS_COMPLETED_RENTAL', 'Completed Rental');
define('DRIVER_STATUS_REMOVED_APPLICATION', 'Driver Removed Application');
define('DRIVER_STATUS_APPLICATION_EXPIRED', 'Application Expired');

//HYR-179 profile
define('PROFILE_TYPE_DRIVER', 'Driver');
define('PROFILE_TYPE_OWNER', 'Owner');
define('USER_STATUS_NAME_DRIVER', 'Driver Status');
define('USER_STATUS_NAME_OWNER', 'Owner Status');
define('DEFAULT_RENTAL_ID', '0');


class Consts {

	static $states = ['AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY','AS','DC','GU','MP','PR','VI'];
	static $platforms = ['uberx','uberblk','instacart','doordash','postmates','caviar','taskrabbit','lyft','homejoy','handy'];
	static $vehicle_types = ['scooter','car','suv'];
	static $rental_periods = ['halfday','fullday','weekly'];
	static $shifts = ['morning_shift','night_shift','all_day'];
	static $licensing = [ CAR_LICENSING_PERSONAL, CAR_LICENSING_COMMERCIAL, CAR_LICENSING_LIVERY, CAR_LICENSING_LEASING ];
	static $paymentStructures = [ 'flatfee','revenuesplit' ];
	static $dows = ['sun','mon','tue','wed','thu','fri','sat'];
	static $bg_check_statuses = [ BG_CHECK_NOTAPPLIED, BG_CHECK_APPLIED, BG_CHECK_CLEAR, BG_CHECK_CONSIDER ];
	static $blockEmails = [EMAIL_MAILINATOR];
	static $timeslots = [
		1 => '12am-1am',
		2 => '1am-2am',
		3 => '2am-3am',
		4 => '3am-4am',
		5 => '4am-5am',
		6 => '5am-6am',
		7 => '6am-7am',
		8 => '7am-8am',
		9 => '8am-9am',
		10 => '9am-10am',
		11 => '10am-11am',
		12 => '11am-12pm',
		13 => '12pm-1pm',
		14 => '1pm-2pm',
		15 => '2pm-3pm',
		16 => '3pm-4pm',
		17 => '4pm-5pm',
		18 => '5pm-6pm',
		19 => '6pm-7pm',
		20 => '7pm-8pm',
		21 => '8pm-9pm',
		22 => '9pm-10pm',
		23 => '10pm-11pm',
		24 => '11pm-12am',
	];

	static function set2int( $const_arr, $set_arr ) {

		$ret = 0;
		$l = count( $const_arr );

		for ( $i = 0; $i < $l; $i++ ) {

			if ( in_array( $const_arr[ $i ], $set_arr ) ) $ret |= ( 1 << $i );

		}

		return $ret;
	}
    static $mine_type = array('jpg' => 'jpg', 'jpge' => 'jpg', 'png' => 'png', 'gif' => 'gif', 'bmp' => 'bmp');


	static $typeSettingPoint = [
		SORT_TYPE_LAST_ACTIVE  => 'Last Login Date',
        SORT_TYPE_LOCALTION    => 'Destination',
        SORT_TYPE_WEEKLY_PRICE => 'Weekly Price',
        SORT_TYPE_PRIOR_RENTAL => 'Prior Rental'
	];

	/**
	 * account access to 2 button 'Extend' && 'Rental Again' in Manage Page
	 * @var array
	 */

};

// End.
