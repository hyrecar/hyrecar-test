<?php

date_default_timezone_set('America/New_York');

$low_host = strtolower( isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : 'cron' );

define('APP_MODE_LOCAL', 0);
define('APP_MODE_DEPLOY', 1);

define('DATE_FORMAT','Y-m-d');
define('DATE_TIME_FORMAT','Y-m-d H:i:s');

define('URL_AWS_BUCKET', 'https://s3.amazonaws.com/hyrecar');

if ( strstr( $low_host, 'localhost' ) !== FALSE ) {

	///////////////////////////////////////////////////////////////////////////
	// Local
	///////////////////////////////////////////////////////////////////////////

	define('APP_MODE', APP_MODE_LOCAL);

	error_reporting( E_ALL & ~E_DEPRECATED );
	ini_set( 'display_errors', 1 );

	define('JWT_KEY', '105e9e1d05e6f7e46b714c47b66df46f');
	define('JWT_PASS_KEY', 'd0c3deb475287151369af32d6a2843b4');
	define('UID_KEY', 'fa8a834998f258c4ae847dc3b757111416ba323a675171111c8f4d7ada00a427');
	define('UID_ALG', MCRYPT_RIJNDAEL_256 );

	define('STRIPE_TOKEN_URI', 'https://connect.stripe.com/oauth/token');
    define('STRIPE_SECRET_KEY', 'sk_test_NWkDuiMJYnzIlckWpy1gdVVs');
    define('STRIPE_PUBLISH_KEY', 'pk_test_Rq1PU7ynHPnynmf46wAzhEo9');
	define('STRIPE_CLIENT_ID', 'ca_65vvcMaaaZ7XSEM4hAtSaFhSNIWoz1AM');
    define('STRIPE_TRANSFER_SCHEDULE_DELAY_DAYS', 7);
    define('STRIPE_TRANSFER_SCHEDULE_INTERVAL', 'daily');
    define('STRIPE_TRANSFER_SCHEDULE_WEEKLY_ANCHOR', 'monday');

	define('CHECKR_API_KEY', '08af1b580748867aa8e6825dced2105e2a8c61cd');
	define('CHECKR_PACKAGE', 'mvr');

	// Twilio Test Account SID and Auth Token from www.twilio.com/user/account
    define('TWILIO_SID', 'AC13463caea1ec22d71e218d403bd6a21f');
    define('TWILIO_TOKEN', '5114fcceb3b7985b13496370dd923db3');
    define('TWILIO_MESSAGING_SERVICE', 'MGccb7f6a99090a1a86d0e06758e1f5062');
    define('TWILIO_NUMBER', '+19093216160');
    define('TWILIO_NUMBER_FOR_MANAGE', '+14152003246');

	define('FIREBASE_SECRET', 'uCdAvJLn60u6Zs3CyDKJ6rya69BZFOA9mMNJXqnn');
    define('FIREBASE_SALT', 'RANDOM_STRING');

	define('DB_SERVER', '127.0.0.1');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	define('DB_NAME', 'hyrecar');
	define('DB_PREFIX', 'hcar_');

	if (!defined('DS')) define('DS','/');

	define( 'RELATIVE_PATH', DS .'rest'. DS );

	define('CURR_DOMAIN', isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : '' );
	define('DEF_SCHEME', 'http://');
	define('SITE_URL', DEF_SCHEME . CURR_DOMAIN . RELATIVE_PATH);
	define('REST_REALM', 'hyrecar.localhost/rest');
	define('DRIVERS_REALM', 'localhost:9001' );
	define('PARTNERS_REALM', 'localhost:9002' );

	define('SITE_NAME', CURR_DOMAIN );
	define('SUPPORT_EMAIL', 'support@hyrecar.com' );
	define('SUPPORT_PHONE', '' );
	define('OPERATIONS_EMAIL', 'driversupport@hyrecar.com');
	$config_path_arr = pathinfo( __FILE__ );
	$config_path_arr = pathinfo( $config_path_arr['dirname'] );

	define('SITE_PATH', $config_path_arr['dirname'] );


	define('ASKTOAPPLY_LIMIT_CNT', 25 );
	define('ASKTOAPPLY_LIMIT_PERIOD', 24 ); //hours

	define('GEO_DEF_RADIUS', 400 ); // mi
    define('GEO_DEF_RADIUS_CAR_SIMILAR', 30 ); // mi

	define('PHOTO_DEF_SMALL_WIDTH', 550 );
	define('PHOTO_DEF_WIDTH', 1080 );

	define('PATH_CHECKR_LOGS', '/logs/checkr.logs' );

    define('PATH_TMP', SITE_PATH . DS .'files' );
    define('PATH_FILES', 'files' );
    define('PATH_CONTRACTS', SITE_PATH . DS .'contracts' );
    define('PATH_PHOTOS', SITE_PATH . DS .'photos' );
    define('PATH_PHOTOS_USERS', 'users' );
    define('PATH_PHOTOS_CARS', 'cars' );
    define('PATH_PHOTOS_CARS_PICKUP', 'cars_pickup' );
    define('PATH_PHOTOS_CARS_REGISTRATION', 'cars_registration' );
    define('PATH_PHOTOS_CARS_INSPECTION', 'cars_inspection' );
    define('PATH_PHOTOS_SCREENSHOTS', 'screenshots' );

    define('URL_FILES', URL_AWS_BUCKET . DS . 'files' );
    define('URL_PHOTOS', DEF_SCHEME . REST_REALM . DS . 'photos' );
    define('URL_PHOTOS_USERS', URL_AWS_BUCKET . DS .'users' );
    define('URL_PHOTOS_CARS', URL_AWS_BUCKET . DS .'cars' );
    define('URL_PHOTOS_CARS_PICKUP', URL_AWS_BUCKET . DS .'cars_pickup' );
    define('URL_PHOTOS_CARS_REGISTRATION', URL_AWS_BUCKET . DS .'cars_registration' );
    define('URL_PHOTOS_CARS_INSPECTION', URL_AWS_BUCKET . DS .'cars_inspection' );
    define('URL_PHOTOS_SCREENSHOTS', URL_AWS_BUCKET . DS .'screenshots' );

    //HYR-761 integrate stripe to zoho
    define('ZOHO_CRM_AUTHEN_KEY', 'e201ff5926aaaf5eefad3870aaac2ec4');
    define('ZOHO_REPORT_AUTHEN_KEY', '');
    // HYR-967 hotfix update conts in file
    define('ZOHO_MODULE_STRIPE_TRANSACTION', 'CustomModule7');
    //HYR 179 push statuses to zoho
    define('ZOHO_MODULE_USER_STATUS', 'Potentials');

		//zendesk creds
		define('ZENDESK_SUBDOMAIN', 'hyrecar');
		define('ZENDESK_USERNAME', 'admin@hyrecar.com');
		define('ZENDESK_TOKEN', 'C6fA83gIkkwLiccNcKQG1pUcXawydE92fXIy2UNG');

		//autopilot api KEY
		define('AUTOPILOT_API_KEY', 'autopilotapikey:f92997ad3d25400ba7f585cfc0a03f80');

        //HYR-1077 fix issue1 of coupon
        define('COUPON_BACKGROUNDCHECK', 'FREEBGC');
} else {

	///////////////////////////////////////////////////////////////////////////
	// Deploy
	///////////////////////////////////////////////////////////////////////////

	define('APP_MODE', APP_MODE_DEPLOY);

	error_reporting( E_ALL & ~E_DEPRECATED );
	ini_set( 'display_errors', 1 );

	define('JWT_KEY', '105e9e1d05e6f7e46b714c47b66df46f');
	define('JWT_PASS_KEY', 'd0c3deb475287151369af32d6a2843b4');
	define('UID_KEY', 'fa8a834998f258c4ae847dc3b757111416ba323a675171111c8f4d7ada00a427');
	define('UID_ALG', MCRYPT_RIJNDAEL_256 );

	define('STRIPE_TOKEN_URI', 'https://connect.stripe.com/oauth/token');
    define('STRIPE_SECRET_KEY', 'sk_test_NWkDuiMJYnzIlckWpy1gdVVs');
    define('STRIPE_PUBLISH_KEY', 'pk_test_Rq1PU7ynHPnynmf46wAzhEo9');
    define('STRIPE_CLIENT_ID', 'ca_65vvcMaaaZ7XSEM4hAtSaFhSNIWoz1AM');
    define('STRIPE_TRANSFER_SCHEDULE_DELAY_DAYS', 7);
    define('STRIPE_TRANSFER_SCHEDULE_INTERVAL', 'daily');
    define('STRIPE_TRANSFER_SCHEDULE_WEEKLY_ANCHOR', 'monday');

    define('CHECKR_API_KEY', '08af1b580748867aa8e6825dced2105e2a8c61cd');
    define('CHECKR_PACKAGE', 'mvr');

	// Twilio Live Account SID and Auth Token from www.twilio.com/user/account
    define('TWILIO_SID', 'AC2dc529caf918d77bc24d1e8ac8a37bf0');
    define('TWILIO_TOKEN', '674672371e9ab2ecdc4f7477d786791d');
    define('TWILIO_NUMBER', '+19093216160');
    define('TWILIO_MESSAGING_SERVICE', 'MGccb7f6a99090a1a86d0e06758e1f5062');

    define('FIREBASE_SECRET', 'uCdAvJLn60u6Zs3CyDKJ6rya69BZFOA9mMNJXqnn');
    define('FIREBASE_SALT', 'hyrecar');

	define('DB_SERVER', '127.0.0.1');
	define('DB_USER', 'hyrecar');
	define('DB_PASS', '5zARZMXuUUZ3FhE4');
	define('DB_NAME', 'hyrecar');
	define('DB_PREFIX', '');

	if (!defined('DS')) define('DS','/');

	define( 'RELATIVE_PATH', DS );

	define('CURR_DOMAIN', isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : '' );
	define('DEF_SCHEME', 'https://');
	define('SITE_URL', DEF_SCHEME . CURR_DOMAIN . RELATIVE_PATH);
	define('REST_REALM', 'rest.hyrecar.com');
	define('DRIVERS_REALM', 'driver.hyrecar.com' );
	define('PARTNERS_REALM', 'partner.hyrecar.com' );
	define('SITE_NAME', CURR_DOMAIN );
	define('SUPPORT_EMAIL', 'support@hyrecar.com' );
    define('SUPPORT_PHONE', '' );
	define('OPERATIONS_EMAIL', 'driversupport@hyrecar.com');

	$config_path_arr = pathinfo(__FILE__);
	$config_path_arr = pathinfo($config_path_arr['dirname']);

	define('SITE_PATH', $config_path_arr['dirname'] );

	define('ASKTOAPPLY_LIMIT_CNT', 5 );
	define('ASKTOAPPLY_LIMIT_PERIOD', 5 ); // minutes

	define('GEO_DEF_RADIUS', 400 ); // mi

	define('PHOTO_DEF_SMALL_WIDTH', 550 );
	define('PHOTO_DEF_WIDTH', 1080 );

	define('PATH_CHECKR_LOGS', '/logs/checkr.logs' );

    define('PATH_TMP', SITE_PATH . DS .'files' );
    define('PATH_FILES', 'files' );
    define('PATH_CONTRACTS', SITE_PATH . DS .'contracts' );
    define('PATH_PHOTOS', SITE_PATH . DS .'photos' );
    define('PATH_PHOTOS_USERS', 'users' );
    define('PATH_PHOTOS_CARS', 'cars' );
    define('PATH_PHOTOS_CARS_PICKUP', 'cars_pickup' );
    define('PATH_PHOTOS_CARS_REGISTRATION', 'cars_registration' );
    define('PATH_PHOTOS_CARS_INSPECTION', 'cars_inspection' );
    define('PATH_PHOTOS_SCREENSHOTS', 'screenshots' );

    define('URL_FILES', URL_AWS_BUCKET . DS . 'files' );
    define('URL_PHOTOS', DEF_SCHEME . REST_REALM . DS . 'photos' );
    define('URL_PHOTOS_USERS', URL_AWS_BUCKET . DS .'users' );
    define('URL_PHOTOS_CARS', URL_AWS_BUCKET . DS .'cars' );
    define('URL_PHOTOS_CARS_PICKUP', URL_AWS_BUCKET . DS .'cars_pickup' );
    define('URL_PHOTOS_CARS_REGISTRATION', URL_AWS_BUCKET . DS .'cars_registration' );
    define('URL_PHOTOS_CARS_INSPECTION', URL_AWS_BUCKET . DS .'cars_inspection' );
    define('URL_PHOTOS_SCREENSHOTS', URL_AWS_BUCKET . DS .'screenshots' );

    //HYR-761 integrate stripe to zoho
    define('ZOHO_CRM_AUTHEN_KEY', 'e37e2638ec6286f4cc674d2788be7e25');
    define('ZOHO_REPORT_AUTHEN_KEY', '');
    // HYR-967 hotfix update conts in file
    define('ZOHO_MODULE_STRIPE_TRANSACTION', 'CustomModule7');
    //HYR 179 push statuses to zoho
    define('ZOHO_MODULE_USER_STATUS', 'Potentials');

		//zendesk creds
		define('ZENDESK_SUBDOMAIN', 'hyrecar');
		define('ZENDESK_USERNAME', 'admin@hyrecar.com');
		define('ZENDESK_TOKEN', 'C6fA83gIkkwLiccNcKQG1pUcXawydE92fXIy2UNG');

		//autopilot api KEY
		define('AUTOPILOT_API_KEY', 'autopilotapikey:f92997ad3d25400ba7f585cfc0a03f80');

		//HYR-1077 fix issue1 of coupon
        define('COUPON_BACKGROUNDCHECK', 'FREEBGC');

}

$CORS_allowed_origins = [
	'hyrecar.com',
	'admin.hyrecar.com',
	'driver.hyrecar.com',
	'partner.hyrecar.com',
	'admin.test.hyrecar.com',
	'driver.test.hyrecar.com',
	'partner.test.hyrecar.com',

	'admin.hyrecar.localhost',
	'driver.hyrecar.localhost',
	'partner.hyrecar.localhost',
	'localhost',
	'localhost:9001',
	'localhost:9002',
	'localhost:9003'
];

define('GOOGLE_GEO_API_KEY', 'AIzaSyD5EbsQ1mRVTX1rhD878KIdXzjzeheXs3I' );
define( 'LANG', 'en' );
define( 'SESSION_NAME', 'HYRECAR_SESSION' );

define('POLICY_NO', '2016ABIDEMO');
define('API_KEY', 'LauFWLxxdWTVY6Ggdt1PQYXtkW3PgqQfWlCX0IbRa8PnICkbm1QS5Zm8vYHL46vh6JHFPmRF1ca');
define('SEQUENCE_NO','233002');

define('SUPPORT_MARKETING_EMAIL_1', 'mike@hyrecar.com' );
define('SUPPORT_MARKETING_NAME_1', 'Mike' );
define('SUPPORT_MARKETING_EMAIL_2', 'erin@hyrecar.com' );
define('SUPPORT_MARKETING_NAME_2', 'Erin' );
define('SUPPORT_MARKETING_EMAIL_3', 'carlos@hyrecar.com' );
define('SUPPORT_MARKETING_NAME_3', 'Carlos' );

define('DOMPDF_ENABLE_AUTOLOAD', false);
define('URL_POLICIES', 'http://hyrecar.com/policies');

define('AWS_KEY', 'AKIAIKIDPZWI4BQSBJOQ');
define('AWS_SECRET_KEY', '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');
define('AWS_ACCOUNT_ID', '1107-3449-8321');
define('REGION', 'us-east-1');
define('VERSION', 'latest');
define('BUCKET', 'hyrecar');


define('SLACKWEBHOOK', 'https://hooks.slack.com/services/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');


$driverRoundRobin	= ["drew@hyrecar.com", "markus@hyrecar.com", "reese@hyrecar.com", "arman@hyrecar.com", "nicholas@hyrecar.com", "sean@hyrecar.com", "eddie@hyrecar.com","nicole@hyrecar.com","frankie@hyrecar.com","ryan@hyrecar.com","daniel@hyrecar.com"];
$ownerRoundRobin	= ["nil@hyrecar.com", "adam@hyrecar.com", "zeight@hyrecar.com"];

// hotfix HYR-135.
$accountAccessToExtend = ['rbar@hyrecar.com', 'megan@hyrecar.com', 'carlos@hyrecar.com'];
$accountAccessToActiveRental = ['admin@hyrecar.com', 'megan@hyrecar.com'];



$extra_driver_round_robin = [
    // zoho id            => email
    '2065895000000104007' => 'abhi@hyrecar.com',
    '2065895000000121107' => 'mike@hyrecar.com',
    '2065895000000284001' => 'nate.ryan@hyrecar.com',
    '2065895000000299158' => 'drew@hyrecar.com',
    '2065895000000299178' => 'reese@hyrecar.com',
    '2065895000000299184' => 'markus@hyrecar.com',
    '2065895000000471021' => 'nil@hyrecar.com',
    '2065895000000471027' => 'arman@hyrecar.com',
    '2065895000000513001' => 'sean@hyrecar.com',
    '2065895000000513055' => 'jazen@hyrecar.com',
    '2065895000000526109' => 'nicholas@hyrecar.com',
    '2065895000000745491' => 'rbar@hyrecar.com',
    '2065895000000860009' => 'carlos@hyrecar.com',
    '2065895000001405018' => 'zeight@hyrecar.com',
    '2065895000001723003' => 'eddie@hyrecar.com',
    '2065895000001765006' => 'megan@hyrecar.com',
    '2065895000002810507' => 'nicole@hyrecar.com',
    '2065895000003373133' => 'support@hyrecar.com',
    '2065895000003513021' => 'frankie@hyrecar.com',
    '2065895000003787432' => 'lily@hyrecar.com',
    '2065895000003821220' => 'angelo@hyrecar.com',
    '2065895000004083141' => 'ryan@hyrecar.com',
    '2065895000004352001' => 'daniel@hyrecar.com',
    '2065895000005979003' => 'griffin@hyrecar.com',
    '2065895000006079001' => 'jim@hyrecar.com',
    '2065895000006250071' => 'angel@hyrecar.com',
    '2065895000006497201' => 'xena@hyrecar.com',
    '2065895000006924172' => 'danny@hyrecar.com',
    '2065895000007134800' => 'vince@hyrecar.com'
];

// HYR-191
define('ZOHO_CRM_MODULE_CONTACT', 'Contacts');
define('ZOHO_CRM_MODULE_LEAD', 'Leads');
define('ZOHO_CRM_MODULE_ACCOUNT','Accounts');
define('ZOHO_CRM_MODULE_USER', 'Users');

define('ZOHO_REPORT_DB', 'Zoho CRM Reports');
define('ZOHO_REPORT_TABLE_RENTALS', 'Rentals');
define('ZOHO_REPORT_TABLE_RENTALS_EXTENSIONS', 'Rentals_Extensions');

define('HYRECAR_SALE_EMAIL', 'sales@hyrecar.com');
define('HYRECAR_DEFAULT_AGENT_EMAIL', 'abhi@hyrecar.com'); // use it when the agent email is HYRECAR_SALE_EMAIL
$extra_driver_round_robin = [
   // zoho id            => email
   '2065895000000104007' => 'abhi@hyrecar.com',
   '2065895000000121107' => 'mike@hyrecar.com',
   '2065895000000284001' => 'nate.ryan@hyrecar.com',
   '2065895000000299158' => 'drew@hyrecar.com',
   '2065895000000299178' => 'reese@hyrecar.com',
   '2065895000000299184' => 'markus@hyrecar.com',
   '2065895000000471021' => 'nil@hyrecar.com',
   '2065895000000471027' => 'arman@hyrecar.com',
   '2065895000000513001' => 'sean@hyrecar.com',
   '2065895000000513055' => 'jazen@hyrecar.com',
   '2065895000000526109' => 'nicholas@hyrecar.com',
   '2065895000000745491' => 'rbar@hyrecar.com',
   '2065895000000860009' => 'carlos@hyrecar.com',
   '2065895000001405018' => 'zeight@hyrecar.com',
   '2065895000001723003' => 'eddie@hyrecar.com',
   '2065895000001765006' => 'megan@hyrecar.com',
   '2065895000002810507' => 'nicole@hyrecar.com',
   '2065895000003373133' => 'support@hyrecar.com',
   '2065895000003513021' => 'frankie@hyrecar.com',
   '2065895000003787432' => 'lily@hyrecar.com',
   '2065895000003821220' => 'angelo@hyrecar.com',
   '2065895000004083141' => 'ryan@hyrecar.com',
   '2065895000004352001' => 'daniel@hyrecar.com',
   '2065895000005979003' => 'griffin@hyrecar.com',
   '2065895000006079001' => 'jim@hyrecar.com',
   '2065895000006250071' => 'angel@hyrecar.com',
   '2065895000006497201' => 'xena@hyrecar.com',
   '2065895000006924172' => 'danny@hyrecar.com',
   '2065895000007134800' => 'vince@hyrecar.com'
];
// Password reset only for admin
$accountAccessToAdmin = ['admin@hyrecar.com'];

//HYR-179 layout id
define('LAYOUT_CAR_ZOHO_CRM_MODULE_STATUS_USER', '1');
define('LAYOUT_DRIVER_ZOHO_CRM_MODULE_STATUS_USER', '2');
define('LAYOUT_RENTAL_ZOHO_CRM_MODULE_STATUS_USER', '3');
define('LAYOUT_OWNER_ZOHO_CRM_MODULE_STATUS_USER', '4');
$zoho_status_user_layout = [
    LAYOUT_CAR_ZOHO_CRM_MODULE_STATUS_USER => '2065895000005612595',
    LAYOUT_DRIVER_ZOHO_CRM_MODULE_STATUS_USER => '2065895000000091023',
    LAYOUT_RENTAL_ZOHO_CRM_MODULE_STATUS_USER => '2065895000006924151',
    LAYOUT_OWNER_ZOHO_CRM_MODULE_STATUS_USER => '2065895000005375945'
];
$zoho_status_user_name = [
    LAYOUT_CAR_ZOHO_CRM_MODULE_STATUS_USER => 'Car Stage',
    LAYOUT_DRIVER_ZOHO_CRM_MODULE_STATUS_USER => 'Driver Stage',
    LAYOUT_RENTAL_ZOHO_CRM_MODULE_STATUS_USER => 'Rental Stage',
    LAYOUT_OWNER_ZOHO_CRM_MODULE_STATUS_USER => 'Owner Stage'
];

//Send to Pending verification button access from Failed verification driver page
$accountAccessForSendToPendingVerify = ['admin@hyrecar.com','carlos@hyrecar.com'];

define('GOOGLE_VISION_PROJECT_ID','seismic-hexagon-163019');

putenv('GOOGLE_APPLICATION_CREDENTIALS='.SITE_PATH.'/inc/googlevision.json');


define('MAPBOX_ACCESS_TOKEN', 'pk.eyJ1IjoibWF5dXJsb2hpdGUiLCJhIjoiY2l6Z2ZzbmcyMDE5NTMybWlobGVlcnNxeSJ9.XKlOHlBmaMJMVb-DxdMkYA');

// End.
