<?php
/**
 * Created by PhpStorm.
 * User: Alex Samofalov
 * Date: 3/8/17
 * Time: 4:39 PM
 */

namespace HyreCar\REST;


/**
 * Class DB
 * @package HyreCar\REST
 */
final class DB
{
    /**
     * Connection to master db server
     * @var null
     */
    protected static $master = null;

    /**
     * Connection to slave db server
     * @var null
     */
    protected static $replication = null;

    /**
     * Connection to cache server
     * @var null
     */
    protected static $cache = null;

    /**
     * prevent the instance from being created (which would create a second instance of it)
     */
    private function __construct() {}

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone() {}

    /**
     * Get database config from ini file configuration
     * @param $databaseName string
     * @return array
     */
    static public function getDatabaseConfig($databaseName){
        $options = [
            'driver' => 'mysqli',
            'host' => '',
            'username' => '',
            'password' => '',
            'database' => '',
            'result' => [
                'formatDate' => DATE_FORMAT,
                'formatDateTime' => DATE_TIME_FORMAT
            ]
        ];

        $file = dirname(__FILE__) . "/../inc/database.ini";
        $ini = parse_ini_file($file, true);

        if (isset($ini[$databaseName])){

            if (isset($ini[$databaseName]['host'])){
                $options['host'] = $ini[$databaseName]['host'];
            }

            if (isset($ini[$databaseName]['username'])){
                $options['username'] = $ini[$databaseName]['username'];
            }

            if (isset($ini[$databaseName]['password'])){
                $options['password'] = $ini[$databaseName]['password'];
            }

            if (isset($ini[$databaseName]['dbname'])){
                $options['database'] = $ini[$databaseName]['dbname'];
            }

            if (isset($ini[$databaseName]['formatdate'])){
                $options['formatDate'] = $ini[$databaseName]['formatdate'];
            }

            if (isset($ini[$databaseName]['formatdatetime'])){
                $options['formatDateTime'] = $ini[$databaseName]['formatdatetime'];
            }
        }

        return $options;
    }

    /**
     * Get cache config from ini file configuration
     * @param $serverName
     * @return array
     */
    static public function getCacheConfig($serverName){
        $options = [
            'host'=>'',
            'password'=>''
        ];

        $file = dirname(__FILE__) . "/../inc/database.ini";
        $ini = parse_ini_file($file, true);

        if (isset($ini[$serverName])){

            if (isset($ini[$serverName]['host'])){
                $options['host'] = $ini[$serverName]['host'];
            }

            if (isset($ini[$serverName]['password'])){
                $options['password'] = $ini[$serverName]['password'];
            }
        }

        return $options;
    }

    /**
     * Connection to master db server
     * @return \DibiConnection
     */
    static public function master(){
        if(!self::$master){
            self::$master = new \DibiConnection(self::getDatabaseConfig('masterdb'));
            self::$master->query("SET SESSION sql_mode = '';");
        }
        return self::$master;
    }


    /**
     * Connection to slave db server (READ ONLY)
     * @return \DibiConnection
     */
    static public function replication(){
        if(!self::$replication){
            self::$replication = new \DibiConnection(self::getDatabaseConfig('slavedb'));
            self::$replication->query("SET SESSION sql_mode = '';");
        }
        return self::$replication;
    }

    /**
     * Connection to cache server
     * @return \Redis
     */
    static public function cache(){
        if(!self::$cache){
            $configuration = self::getCacheConfig('redis');
            self::$cache = new \Redis();
            self::$cache->connect($configuration['host']);
            if (strlen($configuration['password']) > 0) {
                self::$cache->auth($configuration['password']);
            }
        }
        return self::$cache;
    }
}