<?php
require_once 'inc/config.inc.php';
require_once SITE_PATH .'/inc/init.php';
require_once SITE_PATH .'/inc/init.app.php';

$db = \HyreCar\REST\DB::master();

//
// App routes
//

$app->get('/info', function() use ($app) {

	$responseCode = isset( $responseCode ) ? $responseCode : 200;
	$response = isset( $response ) ? $response : [ 'data' => [ 'name' => 'HyreCar Recurting Server 1.0' ] ];

    $app->render( $responseCode, $response );

} );


/**
* require your routes here
*/

