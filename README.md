# HyreCar Recuriting

This test is guaging if can trouble suit a problem and affectly fix your issue, you are allowed to use outside source to figure it out i.e google.

##Rules
1. Setup local enviroment using the LEMP stack (the instruction are limited intentionally).
2. Create am api route that pulls data from database, using the version of Slim we have set furth.
3. Have your admin frontend display data in a sensuable manor.

take this test as far as you feel like going in.
Most important be creative we want to see how you can think outside the box

## Prerequisites

You first need to make sure you have the following tools installed and available in your path.

* [GIT](http://msysgit.github.io/) - a distributed version control system (`latest`)
* [Homebrew](https://brew.sh/) - a package manager for macOS (`latest`)
* [NodeJS](http://nodejs.org/) - a JavaScript runtime built on Chrome's V8 JavaScript engine (`latest`)
* [Grunt](http://gruntjs.com/) - a JavaScript Task Runner (`latest`)
* [Bower](http://bower.io/) - a package manager for the web (`latest`)
* [Composer](https://getcomposer.org/) - a dependency Manager for PHP (`latest`)
* [Ruby](https://www.ruby-lang.org/en/) - Ruby is needed for some of the grunt packages like Compass (`2.0`)
* [Compass](http://compass-style.org/) - a CSS Authoring Framework (`latest`)
* [PHP](http://php.net/) - a popular general-purpose scripting language (`7.x`)
* [nginx](http://nginx.org/en/) (`latest`) - a web-server.
* [MySQL](http://www.mysql.com/downloads/) - a database engine (`5.*`)

We might provide an easy to install development environment in the near future but for now, you need to make sure the above tools are installed.

## Environment setup

### Mac (OS X El Capitan)

## MEMP Setup

Before any of this will work you'll need to have both Xcode Command Line tools and [Homebrew](http://brew.sh/) installed. Here are a few shortcuts to get you started:

```bash
xcode-select --install
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

## Nginx

Files numbered `02` through `07` in ``hyrecar/_docs/sample-configs/nginx`` are [Nginx](http://nginx.org/) configuration files. If you haven't already, install via Homebrew:

```bash
brew install nginx
```

### Running on Port 80

To run Nginx on as your primary web server on port 80, the configuration files need to be owned and run as the user `root`. I have found it easiest to use the system's `LaunchAgents` directory rather than the one in my user's folder -- in fact, it may be required.

```bash
sudo cp /usr/local/opt/nginx/*.plist /Library/LaunchAgents
sudo chown root:wheel /Library/LaunchAgents/*.plist
```

## PHP

Files numbered `08` through `10` in ``hyrecar/_docs/sample-configs/php`` are [PHP-FPM](http://php-fpm.org/) configuration files. If you haven't already, install via Homebrew:

```bash
brew tap homebrew/php
brew install php70 --with-fpm --without-apache --with-homebrew-curl --without-snmp
brew install php70-intl
brew install php70-gd
brew install php70-mbstring
brew install php70-mcrypt
brew install php70-opcache
brew install php70-xdebug
```

### Change PHP Versions

As a developer you may find that you need to change between versions of PHP. Homebrew makes this process easy! The basic steps are to `brew unlink` the current version of PHP and link another installed version.

```bash
brew unlink php70
```

## MySQL

Check what version mysql is first, you will need this number later.
```bash
brew info mysql 
brew install mysql
```
To have launchd start mysql at login
```
ln -sfv /usr/local/opt/mysql/*.plist ~/Library/LaunchAgents
```
To load mysql immediately
```
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mysql.plist
```
Finally add the mysql directory to your PATH environment variable in ``.bash_profile``
```bash
export MYSQL_PATH=/usr/local/Cellar/mysql/5.x.xx  
export PATH=$PATH:$MYSQL_PATH/bin
```
  
Reload shell and type ``mysql -u root`` to confirm
Quit the mysql CLI via ``mysql> \q `` or ``mysql> exit``